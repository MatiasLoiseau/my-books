\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Machine Learning}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Python: Real World Machine Learning}{4}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}The Realm of Supervised Learning}{4}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Constructing a Classifier}{4}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}Predictive Modeling}{4}{subsubsection.1.1.3}
\contentsline {subsubsection}{\numberline {1.1.4}Clustering with Unsupervised Learning}{4}{subsubsection.1.1.4}
\contentsline {subsubsection}{\numberline {1.1.5}Building Recommendation Engines}{4}{subsubsection.1.1.5}
\contentsline {subsubsection}{\numberline {1.1.6}Analyzing Text Data}{4}{subsubsection.1.1.6}
\contentsline {subsubsection}{\numberline {1.1.7}Speech Recognition}{4}{subsubsection.1.1.7}
\contentsline {subsubsection}{\numberline {1.1.8}Dissecting Time Series and Sequential Data}{4}{subsubsection.1.1.8}
\contentsline {subsubsection}{\numberline {1.1.9}Image Content Analysis}{4}{subsubsection.1.1.9}
\contentsline {subsubsection}{\numberline {1.1.10}Biometric Face Recognition}{4}{subsubsection.1.1.10}
\contentsline {subsubsection}{\numberline {1.1.11}Deep Neural Networks}{4}{subsubsection.1.1.11}
\contentsline {subsubsection}{\numberline {1.1.12}Visualizing Data}{4}{subsubsection.1.1.12}
\contentsline {subsubsection}{\numberline {1.1.13}Unsupervised Machine Learning}{4}{subsubsection.1.1.13}
\contentsline {subsubsection}{\numberline {1.1.14}Deep Belief Networks}{4}{subsubsection.1.1.14}
\contentsline {subsubsection}{\numberline {1.1.15}Stacked Denoising Autoencoders}{4}{subsubsection.1.1.15}
\contentsline {subsubsection}{\numberline {1.1.16}Convolutional Neural Networks}{4}{subsubsection.1.1.16}
\contentsline {subsubsection}{\numberline {1.1.17}Semi-Supervised Learning}{4}{subsubsection.1.1.17}
\contentsline {subsubsection}{\numberline {1.1.18}Text Feature Engineering}{4}{subsubsection.1.1.18}
\contentsline {subsubsection}{\numberline {1.1.19}Feature Engineering Part II}{4}{subsubsection.1.1.19}
\contentsline {subsubsection}{\numberline {1.1.20}Ensemble Methods}{4}{subsubsection.1.1.20}
\contentsline {subsubsection}{\numberline {1.1.21}Additional Python Machine Learning Tools}{4}{subsubsection.1.1.21}
\contentsline {subsubsection}{\numberline {1.1.22}First Steps to Scalability}{4}{subsubsection.1.1.22}
\contentsline {subsubsection}{\numberline {1.1.23}Scalable Learning in Scikit-learn}{4}{subsubsection.1.1.23}
\contentsline {subsubsection}{\numberline {1.1.24}Fast SVM Implementations}{4}{subsubsection.1.1.24}
\contentsline {subsubsection}{\numberline {1.1.25}Neural Networks and Deep Learning}{4}{subsubsection.1.1.25}
\contentsline {subsubsection}{\numberline {1.1.26}Deep Learning with TensorFlow}{4}{subsubsection.1.1.26}
\contentsline {subsubsection}{\numberline {1.1.27}Classification and Regression Trees at Scale}{4}{subsubsection.1.1.27}
\contentsline {subsubsection}{\numberline {1.1.28}Unsupervised Learning at Scale}{4}{subsubsection.1.1.28}
\contentsline {subsubsection}{\numberline {1.1.29}Distributed Environments \IeC {\textendash } Hadoop and Spark}{4}{subsubsection.1.1.29}
\contentsline {subsubsection}{\numberline {1.1.30}Practical Machine Learning with Spark}{4}{subsubsection.1.1.30}
\contentsline {subsubsection}{\numberline {1.1.31}Introduction to GPUs and Theano}{4}{subsubsection.1.1.31}
\contentsline {section}{\numberline {2}asd}{4}{section.2}
